package com.zuitt.example;

import java.util.Arrays;

public class RepetitionControl {
    public static void main(String[] args){
        //[Section] Loops
        //are control Structures that allow code blocks to be executed multiple times

        //While Loop
        //allow for repetitive use of code, similar to for-lops, but are usually used for situations where the content to iterate is indefinite

        //to create the variable that will serve as the basis of our condition

        int x = 11;
        while (x < 10) {
            x++;
            System.out.println("The current value of x is : " + x);

        }

        //Do-While Loop
        //similar to while loop. However, do-while loops always execute atleast once - while loops may not execute/run at all;
        int y = 11;
        do {
            System.out.println("The current value of y is " + y);
            y++;
        } while (y < 10);

        //For-loop:
        //Syntax:
        //for(initialVal; condition/stopper; iteration{codeblock/statement};

        for(int i = 0; i < 10 ; ++i){
            System.out.println("The current value of i is " + i);
        }

        String[] nameArray = {"John", "Paul", "George", "Ringo"};

        //for loops for arrays:
        for(String name : nameArray){
            System.out.println(name);
        }

        String [][] classroom = new String[3][3];
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";

        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        for(int i = 0; i < 3; i++){
            for (int j=0; j<3; j++){
                System.out.println(classroom[i][j]);
            }
        }
    }
}
