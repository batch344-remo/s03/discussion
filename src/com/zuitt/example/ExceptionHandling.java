package com.zuitt.example;

import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args){
        // Exceptions
            // a problem that arises during the execution of a program.
            // it disrupts the normal flow of the program and terminate it abnormally

        Scanner input = new Scanner(System.in);

        // Declare a variable num with the data type int:
        int num = 0;
//
//        System.out.println("Please enter a number: ");
//        num = input.nextInt();
//
//        System.out.println("You have entered: " + num);
//        System.out.println("Hello Batch 344!");


        // Try-catch-finally
        // In try statement, it will try to execute the code/statement inside the code block.
        System.out.println("Please enter a number: ");
        try {
            num = input.nextInt();
        } catch (Exception e) {
            // if the try statement returns an exception, the exception will automatically pass to the catch statement and will execute the code block inside of it.
            System.out.println("Invalid Input");
            e.printStackTrace();
        } finally {
            System.out.println("You have entered: " + num);
        }

        System.out.println("Hello Batch 344!");
    }
}
